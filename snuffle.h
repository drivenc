/*
 * loosely based on djb's reference implementation. modified
 * to xor things in-place. this file is hereby placed in public domain.
 */

#define SNUFFLE_LEN 64
#define SNUFFLE_WORDS (SNUFFLE_LEN/sizeof(u32))

#ifndef ROTL32
#define ROTL32(v,c) ((v<<c)|(v>>(32-c)))
#endif

#define ROTATE(v,c) (ROTL32(v,c))
#define XOR(v,w) ((v) ^ (w))
#define PLUS(v,w) ((u32)((v) + (w)))


/* basic salsa transformation */
static inline void salsa_transform(const int rounds, const u32 *input, u32 *x)
{
  int i;

  memcpy(x, input, 64);
 // return;

  for (i = rounds;i > 0;i -= 2) {
    x[ 4] = XOR(x[ 4],ROTATE(PLUS(x[ 0],x[12]), 7));
    x[ 8] = XOR(x[ 8],ROTATE(PLUS(x[ 4],x[ 0]), 9));
    x[12] = XOR(x[12],ROTATE(PLUS(x[ 8],x[ 4]),13));
    x[ 0] = XOR(x[ 0],ROTATE(PLUS(x[12],x[ 8]),18));
    
    x[ 9] = XOR(x[ 9],ROTATE(PLUS(x[ 5],x[ 1]), 7));
    x[13] = XOR(x[13],ROTATE(PLUS(x[ 9],x[ 5]), 9));
    x[ 1] = XOR(x[ 1],ROTATE(PLUS(x[13],x[ 9]),13));
    x[ 5] = XOR(x[ 5],ROTATE(PLUS(x[ 1],x[13]),18));
    
    x[14] = XOR(x[14],ROTATE(PLUS(x[10],x[ 6]), 7));
    x[ 2] = XOR(x[ 2],ROTATE(PLUS(x[14],x[10]), 9));
    x[ 6] = XOR(x[ 6],ROTATE(PLUS(x[ 2],x[14]),13));
    x[10] = XOR(x[10],ROTATE(PLUS(x[ 6],x[ 2]),18));
    
    x[ 3] = XOR(x[ 3],ROTATE(PLUS(x[15],x[11]), 7));
    x[ 7] = XOR(x[ 7],ROTATE(PLUS(x[ 3],x[15]), 9));
    x[11] = XOR(x[11],ROTATE(PLUS(x[ 7],x[ 3]),13));
    x[15] = XOR(x[15],ROTATE(PLUS(x[11],x[ 7]),18));

    x[ 1] = XOR(x[ 1],ROTATE(PLUS(x[ 0],x[ 3]), 7));
    x[ 2] = XOR(x[ 2],ROTATE(PLUS(x[ 1],x[ 0]), 9));
    x[ 3] = XOR(x[ 3],ROTATE(PLUS(x[ 2],x[ 1]),13));
    x[ 0] = XOR(x[ 0],ROTATE(PLUS(x[ 3],x[ 2]),18));
    
    x[ 6] = XOR(x[ 6],ROTATE(PLUS(x[ 5],x[ 4]), 7));
    x[ 7] = XOR(x[ 7],ROTATE(PLUS(x[ 6],x[ 5]), 9));
    x[ 4] = XOR(x[ 4],ROTATE(PLUS(x[ 7],x[ 6]),13));
    x[ 5] = XOR(x[ 5],ROTATE(PLUS(x[ 4],x[ 7]),18));
    
    x[11] = XOR(x[11],ROTATE(PLUS(x[10],x[ 9]), 7));
    x[ 8] = XOR(x[ 8],ROTATE(PLUS(x[11],x[10]), 9));
    x[ 9] = XOR(x[ 9],ROTATE(PLUS(x[ 8],x[11]),13));
    x[10] = XOR(x[10],ROTATE(PLUS(x[ 9],x[ 8]),18));
    
    x[12] = XOR(x[12],ROTATE(PLUS(x[15],x[14]), 7));
    x[13] = XOR(x[13],ROTATE(PLUS(x[12],x[15]), 9));
    x[14] = XOR(x[14],ROTATE(PLUS(x[13],x[12]),13));
    x[15] = XOR(x[15],ROTATE(PLUS(x[14],x[13]),18));
  }
}

/* encrypt stream of arbitrary length, in-place. count is number of SNUFFLE_LEN blocks */
static	inline void salsa_crypt_inplace(const int rounds, void *data, const int count, const void *inkey, const u64 iv)
{
	int	i = count;
	u32	*ptr = data;
	u32	wkey[SNUFFLE_WORDS];
	u32	hash[SNUFFLE_WORDS];

	/* compute first key */
	memcpy(wkey, inkey, SNUFFLE_LEN);
	*(u64 *) (&wkey[SNUFFLE_WORDS-2]) += iv;
	while (i--) {
		int j;
		salsa_transform(rounds, wkey, hash);
		for (j = 0; j < SNUFFLE_WORDS; ++j)
			ptr[j] ^= PLUS(hash[j], wkey[j]);
		ptr += SNUFFLE_WORDS;
		(*(u64 *) (&wkey[SNUFFLE_WORDS-2]))++;
	}
}

/* encrypt stream of arbitrary length, in-place. count is number of SNUFFLE_LEN blocks */
static	inline void salsa_crypt(const int rounds, const void *data, void *dest, const int count, const void *inkey, const u64 iv)
{
	int	i = count;
	const	u32	*ptr = data;
	u32	*dptr = dest;
	u32	wkey[SNUFFLE_WORDS];
	u32	hash[SNUFFLE_WORDS];

	/* compute first key */
	memcpy(wkey, inkey, SNUFFLE_LEN);
	*(u64 *) (&wkey[SNUFFLE_WORDS-2]) += iv;
	while (i--) {
		int j;
		salsa_transform(rounds, wkey, hash);
		for (j = 0; j < SNUFFLE_WORDS; ++j)
			dptr[j] = ptr[j] ^ PLUS(hash[j], wkey[j]);
		ptr += SNUFFLE_WORDS;
		dptr += SNUFFLE_WORDS;
		(*(u64 *) (&wkey[SNUFFLE_WORDS-2]))++;
	}
}

