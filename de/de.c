/*
 * main control tool
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>


#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t


#define	HASH_ROUNDS	10000
#include "snuffle.h"
#include "linux.h"

volatile int got_alarm = 0;

int	usage(char **argv)
{

	printf("usage: %s -[cm]\n"
	"to create/re-encrypt new key:\n"
	"	%s -c [oldkey]\n"
	"to map drive(s) with key:\n"
	"	%s -m key /dev/device1 /dev/deviceX ..\n"
	"to unmap drive(s):\n"
	"	%s -u /dev/mapper/dev1,/dev/dev2...\n", argv[0], argv[0], argv[0], argv[0]);
	return 1;
}

void	alarm_h(int d)
{
	got_alarm = 1;
}


void	printkey(char *key)
{
	int i;
	for (i = 0; i < SNUFFLE_WORDS; i++) {
		printf("%08x", ((u32 *) key)[i]);
	}
	printf("\n");
}

void	expand_pass(char *pass, char *pass1)
{
	int i;
	int plen;
	strncpy(pass, pass1, SNUFFLE_LEN-1);
	pass[SNUFFLE_LEN-1] = 0;
	plen = strlen(pass);

	//printf("Plen=%d\n", plen);
	/* expand pass */
	if (plen < SNUFFLE_LEN) pass[plen] = plen;
	for (i = plen+1; i < SNUFFLE_LEN; i++) {
		pass[i] = pass[i%plen];
	}
}

void	encrypt_key(char *key, char *pass1)
{
	int i, rounds;
	char tmp[SNUFFLE_LEN], pass[SNUFFLE_LEN];
	char sum;
	expand_pass(pass, pass1);

#if 0
	printf("Encrypting raw key:");
	printkey(key);
	printf("Passphrase:");
	printkey(pass);
#endif

	signal(SIGALRM, alarm_h);
	alarm(1);
	rounds = 0;
	sum = 0;
	do {
		rounds++;
		salsa_transform(HASH_ROUNDS, (void *) pass, (void *) tmp);
		memcpy(pass, tmp, SNUFFLE_LEN);
	} while (!got_alarm);
//	printkey(pass);
	for (i = 0; i < SNUFFLE_LEN; i++) {
		key[i] ^= pass[i];
		sum += pass[i];
	}
	printf("New key is (after %d hash rounds, sum %d):\n%08x%02hhx", rounds, sum, rounds, sum);
	printkey(key);
}

int	decrypt_key(char *key, char *pass1, char sum, int rounds)
{
	int i;
	char tmp[SNUFFLE_LEN], pass[SNUFFLE_LEN];
	expand_pass(pass, pass1);
	for (i = 0; i < rounds; i++) {
		salsa_transform(HASH_ROUNDS, (void *) pass, (void *) tmp);
		memcpy(pass, tmp, SNUFFLE_LEN);
	}
	for (i = 0; i < SNUFFLE_LEN; i++) {
		key[i] ^= pass[i];
		sum -= pass[i];
	}
	if (sum) {
		fprintf(stderr, "Invalid passphrase!\n");
		return 0;
	}
	return 1;
}

char	*load_key(char *keyin)
{
	int i, rounds;
	static char sum, *pass1;
	static char key[SNUFFLE_LEN], tkey[SNUFFLE_LEN];
	if (strlen(keyin) != 2+8+(SNUFFLE_WORDS*8))
		goto sizerr;
	if (sscanf(keyin, "%08x%02hhx", &rounds, &sum) != 2) goto sizerr;
	for (i = 0; i < SNUFFLE_WORDS; i++) {
		if (sscanf(keyin + 10 + i * 8, "%08x", (u32 *) (&key[i*4])) != 1) goto sizerr;
	}

	/* already have a pass - try it */
	if (pass1) {
		memcpy(tkey, key, sizeof(key));
		if (decrypt_key(tkey, pass1, sum, rounds)) return tkey;
	}

	do {
		pass1 = getpass("Enter passphrase:");
		if (!pass1[0]) { printf("Cancelled!\n"); exit(1); }
		memcpy(tkey, key, sizeof(key));
	} while (!decrypt_key(tkey, pass1, sum, rounds));

	return tkey;
sizerr:;
	fprintf(stderr, "Invalid key size!\n");
	exit(1);
}

void	mk_key(char *oldkey)
{
	char	*key;
	char	*pass1, *pass2;

	if (oldkey) {
		key = load_key(oldkey);
	} else {
		/* generate new key .. */
		int fd = open("/dev/urandom", O_RDONLY);
		int i;
		static char buf[SNUFFLE_LEN];
		for (i = 0; i < SNUFFLE_LEN; i++) {
			printf("\rGathering entropy, got %d of %d bits...", i * 8, SNUFFLE_LEN * 8);
			fflush(stdout);
			read(fd, buf + i, 1);
		}
		printf("\rGathering entropy, got %d of %d bits...", i * 8, SNUFFLE_LEN * 8);
		close(fd);
		puts("");
		key = buf;
	}
	while (1) {
		pass1 = getpass("Enter new passphrase:");
		pass2 = getpass("Enter new passphrase again:");
		if (!pass1[0]) { printf("Invalid (none) passphrase!\n"); continue; };
		if (!strcmp(pass1, pass2)) break;
		printf("Passphrases didn't match; try again\n");
	}
	encrypt_key(key, pass1);
	exit(0);
}

int	main(int argc, char *argv[])
{
	int i;
	printf("DE 0.2 (c) 2008 kt@leet.cz\n");
	if (argc < 2 || argv[1][0] != '-' || argv[1][2] != 0) return usage(argv);
	switch (argv[1][1]) {
		case 'c':
			mk_key(argv[2]);
			break;
		case 'm':
			if (!argv[3]) return usage(argv);
			for (i = 2; argv[i] && argv[i+1]; i += 2)
				map_dev(load_key(argv[i]), argv[i+1]);
			break;
		case 'u':
			if (!argv[2]) return usage(argv);
			unmap_dev(&argv[2]);
			break;
		default:
			return usage(argv);
	}
	return 0;
}
