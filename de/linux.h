#include <libdevmapper.h>
#include <sys/ioctl.h>
#include <linux/fs.h>

#define SECTOR_SHIFT 9

u64	dev_size(char *dev)
{
	u64 size;
	u32 size_small;

	int fd = open(dev, O_RDONLY);
	if (fd < 0) {
		perror(dev);
		exit(1);
	}
#ifdef BLKGETSIZE64
	if (ioctl(fd, BLKGETSIZE64, &size) >= 0) {
		size >>= SECTOR_SHIFT;
		return size;
	}
#endif

#ifdef BLKGETSIZE
	if (ioctl(fd, BLKGETSIZE, &size_small) >= 0) {
		return size_small;
	}
#else
#	error "Need at least the BLKGETSIZE ioctl!"
#endif
	perror(dev);
	exit(1);
}

char	*base_name(char *name)
{
	char *p = strrchr(name, '/');
	if (p) return p+1;
	return name;
}


int	map_device(char *key, char *device)
{
	int i;
	struct	dm_task *dmt = dm_task_create(DM_DEVICE_CREATE);
	char	pbuf[1024];
	char	*p = pbuf;

	/* offset 0, 8 rounds */
	p += sprintf(p, "%s %d %d ", device, 0, 8);
	for (i = 0; i < SNUFFLE_LEN; i++) p += sprintf(p, "%02hhx", key[i]);

	if (!dmt) return 0;
	if (!dm_task_set_name(dmt, base_name(device))) return 0;
	if (!dm_task_add_target(dmt, 0, dev_size(device), "drivenc", pbuf)) return 0;
	if (!dm_task_run(dmt)) return 0;
	dm_task_destroy(dmt);
	return 1;
}

int	unmap_device(char *device)
{
	struct	dm_task *dmt = dm_task_create(DM_DEVICE_REMOVE);
	if (!dmt) return 0;
	if (!dm_task_set_name(dmt, base_name(device))) return 0;
	if (!dm_task_run(dmt)) return 0;
	dm_task_destroy(dmt);
	return 1;
}

void	map_dev(char *rawkey, char *dev)
{
	printf("Mapping %s\n", dev);
	if (!map_device(rawkey, dev)) perror(dev);
}

void	unmap_dev(char **devices)
{
	int i;
	for (i = 0; devices[i]; i++) {
		printf("Unmapping %s\n", devices[i]);
		if (!unmap_device(devices[i])) perror(devices[i]);
	}
}


