# linux related

ifeq ($(shell uname),Linux)
all:
	@echo Bulding Linux module...
	make -C linux-kmod
	make -C de
clean:
	make -C linux-kmod clean
	make -C de clean
else
ifeq ($(shell uname),FreeBSD)
all:
	@echo Building FreeBSD module...
	make -C freebsd-kmod
	make -C de
clean:
	make -C freebsd-kmod clean
	make -C de clean
else
	@echo "Only Linux 2.6 and FreeBSD are supported"
endif
endif
