/*
 * verify that our salsa20 implementation is up to spec.
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t

//#include "snuffle.h"
#include "common.h"

uint64_t gett()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000000 + tv.tv_usec;
}


#define	TESTSZ	(1024)

int	main()
{
	uint64_t took,start; 
	static	char	testbuf[TESTSZ];
	static	char	key[SNUFFLE_LEN];

	memset(testbuf, 0, TESTSZ);
	memset(key, 'A', SNUFFLE_LEN);

	start = gett();
//	salsa_crypt_inplace(4, testbuf, 4, key, 1);

	do_decrypt_inplace(testbuf, key, 4, 1, 512);

//	salsa_crypt_inplace(2, testbuf + SNUFFLE_LEN, (TESTSZ/SNUFFLE_LEN)-1, key, 1);
//	salsa_crypt_inplace(2, testbuf, TESTSZ/SNUFFLE_LEN, key, 0);
	took = gett()-start;
//	printf("%d Kb in %lld ms = %lld Mb/s\n", TESTSZ/1000, took/1000, (TESTSZ/took));
	write(2, testbuf, TESTSZ);
}

