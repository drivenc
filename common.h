/* kernel/cipher-independent wrapper */

#define DE_V1 0
#define DE_V2 0
#define DE_V3 1

#include "snuffle.h"

/* decrypt data IN-place */
static	inline	void do_decrypt_inplace(void *data, void *key, int rounds, u64 iv, int len)
{
	salsa_crypt_inplace(rounds, data, len/SNUFFLE_LEN, key, iv/SNUFFLE_LEN);
}

/* encrypt data from src to dst */
static	inline	void do_encrypt(void *src, void *dst, void *key, int rounds, u64 iv, int len)
{
	salsa_crypt(rounds, src, dst, len/SNUFFLE_LEN, key, iv/SNUFFLE_LEN);
}

