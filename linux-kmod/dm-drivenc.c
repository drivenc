/*
 * linux driver
 */

#include <linux/err.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/bio.h>
#include <linux/blkdev.h>
#include <linux/mempool.h>
#include <linux/slab.h>
#include <linux/crypto.h>
#include <linux/workqueue.h>
#include <linux/backing-dev.h>
#include <asm/atomic.h>
#include <linux/scatterlist.h>
#include <asm/page.h>
#include <asm/unaligned.h>
#include <linux/version.h>

#include <dm.h>

#include "../common.h"
#if 0
#include <linux/bio.h>
#include <linux/blkdev.h>
#include <linux/ctype.h>
#include <linux/device-mapper.h>
#include <linux/init.h>
#include <linux/kdev_t.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/workqueue.h>
#include <linux/dm.h>
#endif

#define DB printk
#define	IOPOOL_SIZE	64
/* 512kb */
#define	PGPOOL_SIZE	16

#define GFP_POOL GFP_NOIO
#define WAITALLOC(direction, wtf) while (!(wtf)) congestion_wait(direction, HZ / 50)


struct de_ctx {
	/* our underlying device */
	struct dm_dev *dev;
	/* sector offset we start at */
	sector_t offset;

	/* our secret key */
	int	rounds;
	u32	key[SNUFFLE_WORDS];
};

struct io_ctx {
	struct	dm_target *target;
	struct	bio *bio;
	atomic_t count;
	sector_t sector;
	struct	work_struct work;
};

/* pools */
static struct kmem_cache *io_cache = NULL;
static mempool_t *io_pool = NULL;
static mempool_t *pg_pool = NULL;
static struct workqueue_struct *work_queue = NULL;

/* constructor */
static int drivenc_ctr(struct dm_target *ti, unsigned int argc, char **argv)
{
	struct de_ctx *ctx = NULL;
	int i;
	int error = -EINVAL;
	unsigned long long offset = 0;

	if (argc != 4) {
		ti->error = "drivenc: Use: <real_device> <start_sector> <rounds> <key>";
		goto fail;
	}

	/* get context */
	if (!(ctx = kmalloc(sizeof(*ctx), GFP_KERNEL))) {
		ti->error = "out of memory";
		error = -ENOMEM;
		goto fail;
	}
	memset(ctx, 0, sizeof (*ctx));

	/* dev offset */
	if (sscanf(argv[1], "%llu", &offset)<=0) {
		ti->error = "trouble finding device offset";
		goto fail;
	}
	ctx->offset = offset;


	/* keep cipher rounds within sane range.
	 * XXX: 4 rounds is mere obfuscation, shouldn't we make it harder for user
	 * to use the cipher in such a weak mode? */
	if ((sscanf(argv[2], "%d", &ctx->rounds)<=0) || (ctx->rounds&1)
	    || (ctx->rounds < 4) || (ctx->rounds > 128)) {
		ti->error = "number of rounds should be >= 4 and <= 128";
		goto fail;
	}

	/* key */
	for (i = 0; i < SNUFFLE_WORDS; i++)
		if (sscanf(argv[3] + (i*8), "%08x", &ctx->key[i])!=1) {
			ti->error = "16 words expected for key";
			goto fail;
	}

	/* find source device */
	if (dm_get_device(ti, argv[0], ctx->offset, ti->len,
	    dm_table_get_mode(ti->table), &ctx->dev)) {
		ti->error = "device lookup failed";
		goto fail;
	}
	ti->private = ctx;

	return 0;
fail:
	if (ctx) kfree(ctx);
	return error;
}

/* destructor. */
static void drivenc_dtr(struct dm_target *ti)
{
	struct de_ctx *ctx = ti->private;

	dm_put_device(ti, ctx->dev);
	kfree(ctx);
}

/* call upstream endio once we're done with all requests. */
static void put_io(struct io_ctx *io, int error)
{
	//struct de_ctx *ctx = io->target->private;

	if (!atomic_dec_and_test(&io->count))
		return;

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,24)
	bio_endio(io->bio, io->bio->bi_size, error);
#else
	bio_endio(io->bio, error);
#endif
	mempool_free(io, io_pool);
}

/* decryption worker */
static void decrypt_worker(struct work_struct *w)
{
	struct io_ctx *io = container_of(w, struct io_ctx, work);
	struct de_ctx *ctx = io->target->private;
	struct bio_vec *vector;
	int idx;
	u64 iv = io->sector << SECTOR_SHIFT;

	bio_for_each_segment(vector, io->bio, idx) {
		/* map & decrypt */
		do_decrypt_inplace(kmap(vector->bv_page) + vector->bv_offset,
					ctx->key, ctx->rounds, iv, vector->bv_len);

		/* adjust IV */
		iv += vector->bv_len;
		kunmap(vector->bv_page);
	}

	/* this will call upstream endio once all references are dropped */
	put_io(io, 0);
}

/* called when reading/writing done */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,24)
static int drivenc_endio(struct bio *bio, unsigned int done, int error)
#else
static void drivenc_endio(struct bio *bio, int error)
#endif
{
	struct io_ctx *io = bio->bi_private;
//	struct de_ctx *ctx = io->target->private;
	struct bio_vec *vector;
	int idx;


        if (unlikely(!bio_flagged(bio, BIO_UPTODATE) && !error))
		error = -EIO;
			
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,24)
	/* incomplete. */
	if (bio->bi_size) return 1;
#endif

	/* reading.
	 * TODO: we should probably attempt to determine whether we're inside
	 * interrupt context and perform decryption imediately if not..
	 * that'd save us some overhead of worker-thread switches. */
	if (bio_data_dir(bio) == READ) {
		if (error)
		    goto output;
		bio_put(bio);
		queue_work(work_queue, &io->work);
		goto out;
	} else {
		bio_for_each_segment(vector, bio, idx)
		mempool_free(vector->bv_page, pg_pool);
	}
output:;
	bio_put(bio);
	put_io(io, error);
out:;
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,24)
	return error;
#endif
}

/* this performs the actual I/O request translation */
static int drivenc_map(struct dm_target *ti, struct bio *bio,
			  union map_info *map_context)
{
	struct de_ctx *ctx = ti->private;
	struct io_ctx *io;
	struct bio *clone;
	struct bio_vec *vector;
	int idx;

	//DB("allocation io\n");
	/* our io context */
	if (!(io = mempool_alloc(io_pool, GFP_POOL)))
		return -ENOMEM;

	//DB("allocation bio\n");
	/* TODO: maybe we should poolize this as well */
	WAITALLOC(bio_data_dir(bio), clone = bio_alloc(GFP_NOIO, bio_segments(bio)));

	atomic_set(&io->count, 1);

	io->bio = bio;
	io->target = ti;
	io->sector = ctx->offset + bio->bi_sector;

	clone->bi_bdev = ctx->dev->bdev;
	clone->bi_end_io = drivenc_endio;
	clone->bi_idx = 0;
	clone->bi_private = io;
	clone->bi_rw = bio->bi_rw;
	clone->bi_sector = io->sector;
	clone->bi_size = bio->bi_size;
	clone->bi_vcnt = bio_segments(bio);

	if (bio_data_dir(bio) == READ) {
		/* reuse buffers for reading */
		memcpy(bio_iovec(clone), bio_iovec(bio),
			bio_segments(bio) * sizeof(struct bio_vec));
		INIT_WORK(&io->work, decrypt_worker);
	} else {
		u64 iv = io->sector << SECTOR_SHIFT;
		struct bio_vec *nvec = bio_iovec(clone);

		bio_for_each_segment(vector, bio, idx) {
			WAITALLOC(WRITE, nvec->bv_page = mempool_alloc(pg_pool, GFP_POOL));
			nvec->bv_offset = 0;
			nvec->bv_len = vector->bv_len;

			/* mmap & encrypt pages */
			do_encrypt(kmap(vector->bv_page) + vector->bv_offset,
					kmap(nvec->bv_page),
					ctx->key, ctx->rounds, iv, vector->bv_len);
			/* adjust iv */
			iv += vector->bv_len;
			/* unmap */
			kunmap(vector->bv_page);
			kunmap(nvec->bv_page);
			/* go to next segment */
			nvec++;
		}
	}

	atomic_inc(&io->count);
	generic_make_request(clone);
	put_io(io, 0);
	return 0;
}

static struct target_type de_ops = {
	.name   = "drivenc",
	.version= {DE_V1, DE_V2, DE_V3},
	.module = THIS_MODULE,
	.ctr    = drivenc_ctr,
	.dtr    = drivenc_dtr,
	.map    = drivenc_map,
};


int __init drivenc_init(void)
{
	int r = -ENOMEM;

	if (!(work_queue = create_workqueue ("drivenc"))) goto fail;
	if (!(io_cache = kmem_cache_create("drivenc-bioctx",
		sizeof(struct io_ctx),
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,24)
		0, 0, NULL, NULL
#else
		0, 0, NULL
#endif
		
		))) goto fail;
	
	if (!(pg_pool = mempool_create_page_pool(PGPOOL_SIZE, 0)))
		goto fail;

	if (!(io_pool = mempool_create_slab_pool(IOPOOL_SIZE, io_cache)))
		goto fail;

	if (!(r = dm_register_target (&de_ops)))
		return 0;

fail:
	if (work_queue) destroy_workqueue(work_queue);
	if (io_pool) mempool_destroy(io_pool);
	if (pg_pool) mempool_destroy(pg_pool);
	if (io_cache) kmem_cache_destroy(io_cache);
	return r;
}


void __exit drivenc_exit(void)
{
	if ((dm_unregister_target (&de_ops)) < 0)
		return;
	destroy_workqueue(work_queue);
	mempool_destroy(io_pool);
	mempool_destroy(pg_pool);
	kmem_cache_destroy(io_cache);
}


module_init(drivenc_init);
module_exit(drivenc_exit);

MODULE_AUTHOR("Karel Tuma");
MODULE_DESCRIPTION("DM target for drivenc encryption");
MODULE_LICENSE("GPL");

